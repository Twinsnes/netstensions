﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Threading;
using Binding = System.Windows.Data.Binding;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using KeyEventHandler = System.Windows.Input.KeyEventHandler;

namespace netstensions
{
    /// <summary>
    /// Interaction logic for EditableTreeView.xaml
    /// </summary>
    public partial class EditableTreeView
    {

        /// <summary>
        ///   Counter for doubleclick events to prevent mouse up event handlers
        ///   from executing if there is a double click.
        /// </summary>
        private int _dcCounter;

        /// <summary>
        ///   Keeping track of the last clicked EditableTextBlock
        /// </summary>
        private EditableTextBlock _lastClicked;
        
        /// <summary> 
        ///     ItemsSource specifies a collection used to generate the content of
        /// this control.  This provides a simple way to use exactly one collection 
        /// as the source of content for this control. 
        /// </summary>
        /// <remarks> 
        ///     Any existing contents of the Items collection is replaced when this
        /// property is set. The Items collection will be made ReadOnly and FixedSize.
        ///     When ItemsSource is in use, setting this property to null will remove
        /// the collection and restore use to Items (which will be an empty ItemCollection). 
        ///     When ItemsSource is not in use, the value of this property is null, and
        /// setting it to null has no effect. 
        /// </remarks> 
        [Bindable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IEnumerable ItemsSource
        {
            get { return TView.ItemsSource; }
            set { TView.ItemsSource = value; }
        }

        /// <summary>
        ///   Name of the property that will be used to display
        ///   the text for each TreeViewItem.
        /// 
        ///   Default is "Name".
        /// </summary>
        /// <remarks>
        ///   This is a string representation of a binding path, so 
        ///   it is possible to use a multi level property path.
        ///   <example>Name.Value</example>
        /// </remarks>
        public string PropertyName
        {
            get { return _propertyName; }
            set
            {
                // ignore if there is no difference.
                if (value.Equals(_propertyName))
                {
                    return;
                }

                _propertyName = value;

                CreateHierarchicalDataTemplate();
            }
        }

        /// <summary>
        ///   Private holder for PropertyName property.
        /// </summary>
        private string _propertyName;

        /// <summary>
        ///   Name of the property that contains the child elements
        ///   to display in the tree view.
        /// 
        ///   Default is "Children"
        /// </summary>
        /// <remarks>
        ///   This is a string representation of a binding path, so
        ///   it is possible to use multilevel property path.
        ///   <example>Children.Values</example>
        /// </remarks>
        public string ChildPropertyName
        {
            get { return _childPropertyName; }
            set
            {
                // ignore if there is no difference.
                if (value.Equals(_propertyName))
                {
                    return;
                }

                _childPropertyName = value;

                CreateHierarchicalDataTemplate();
            }
        }

        /// <summary>
        ///   Private holder for ChildPropertyName property.
        /// </summary>
        private string _childPropertyName;

        /// <summary>
        ///   Constructor
        /// </summary>
        public EditableTreeView()
        {
            InitializeComponent();
            _propertyName = "Name";
            _childPropertyName = "Children";

            CreateHierarchicalDataTemplate();

        }

        /// <summary>
        ///   Event handler for MouseDoubleClick event on EditableTextBlock.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditableTextBlockMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            _dcCounter+=2;
            _lastClicked = sender as EditableTextBlock;
        }

        /// <summary>
        ///   Event handler for MouseUp event on an EditableTextBlock.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditableTextBlockMouseUp(object sender, MouseButtonEventArgs e)
        {
            var editableTextBlock = sender as EditableTextBlock;

            if (editableTextBlock == null)
            {
                return;
            }

            // create a timer to wait for the system default 
            // doubleclick time to see if we have a double click or not.
            DispatcherTimer timer = new DispatcherTimer(DispatcherPriority.Normal, Dispatcher);

            // we use the windows forms SystemInformation class as it's easier to work with than p/invoke
            timer.Interval = new TimeSpan(0, 0, 0, 0, SystemInformation.DoubleClickTime);

            timer.Tick += delegate
                              {
                                  timer.Stop();

                                  if (_dcCounter > 0)
                                  {
                                      _dcCounter--;
                                      return;
                                  }
                                  
                                  if (_lastClicked == editableTextBlock)
                                  {
                                      editableTextBlock.IsEditable = true;
                                  }
                                  else
                                  {
                                      _lastClicked = editableTextBlock;
                                      editableTextBlock.Focus();
                                  }
                                  
                                  return;
                              };
            timer.Start();
        }

        /// <summary>
        ///   Event handler for KeyDown event on an EditableTextBlock.
        ///   If 'F2' is pressed it will switch the mode to editable on the
        ///   EditableTextBlock that fired the event.
        /// </summary>
        /// <param name="sender">EditableTextBlock</param>
        /// <param name="e">KeyEventArgs</param>
        private void EditableTextBlockKeyDown(object sender, KeyEventArgs e)
        {
            // validation
            var editableTextBlock = sender as EditableTextBlock;

            if (editableTextBlock == null)
            {
                return;
            }

            switch (e.Key)
            {
                case(Key.F2):
                    editableTextBlock.IsEditable = true;
                    break;
            }
        }

        private void ViewSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {

        }

        /// <summary>
        ///   Creates a new HierarchicalDataTemplate for the internal tree view
        ///   when the property PropertyName or ChildPropertyName is changed.
        /// </summary>
        private void CreateHierarchicalDataTemplate()
        {
            var textBoxFactory = new FrameworkElementFactory(typeof(EditableTextBlock));

            var template = new HierarchicalDataTemplate
                               {
                                   ItemsSource = new Binding("Children")
                               };


            textBoxFactory.SetBinding(EditableTextBlock.TextProperty, new Binding("Name"));

            // set up the event handlers
            textBoxFactory.AddHandler(MouseDoubleClickEvent, new MouseButtonEventHandler(EditableTextBlockMouseDoubleClick));
            textBoxFactory.AddHandler(KeyDownEvent, new KeyEventHandler(EditableTextBlockKeyDown));
            textBoxFactory.AddHandler(MouseUpEvent, new MouseButtonEventHandler(EditableTextBlockMouseUp));



            // attach the textbox to the tempaltes visual tree.
            template.VisualTree = textBoxFactory;


            // attach the template to the tree view
            TView.ItemTemplate = template;

        }
    }
}
