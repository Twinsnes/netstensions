﻿using System.Collections.ObjectModel;
using System.ComponentModel;

namespace netstations.Examples
{
    /// <summary>
    ///   Example item class that implements INotifyPropertyChanged
    /// </summary>
    public class Item : INotifyPropertyChanged
    {
        /// <summary>
        ///   Privte backing field for the name property.
        /// </summary>
        private string _name;

        /// <summary>
        ///   The name of the item.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Name"));
                }
            }
        }

        /// <summary>
        ///   Event fired when a property on this object changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<Item> Children { get; private set; }

        /// <summary>
        ///   Constructor
        /// </summary>
        /// <param name="name">Name describing the item.</param>
        public Item(string name)
        {
            _name = name;
            Children = new ObservableCollection<Item>();
        }
    }
}
