﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace netstensions
{
    /// <summary>
    /// Interaction logic for EditableTextBlock.xaml
    /// </summary>
    public partial class EditableTextBlock
    {

        #region Properties

        /// <summary>
        ///   Dependency property for the IsEditable property.
        /// </summary>
        public static readonly DependencyProperty IsEditableProperty =
            DependencyProperty.Register("IsEditable", typeof (bool), typeof (EditableTextBlock), new PropertyMetadata(default(bool)));

        /// <summary>
        ///   If the control is in edit mode or not.
        /// </summary>
        public bool IsEditable
        {
            get { return (bool) GetValue(IsEditableProperty); }
            set { SetValue(IsEditableProperty, value); }
        }

        /// <summary>
        ///   Dependency property for the IsReadOnly property.
        /// </summary>
        public static readonly DependencyProperty IsReadOnlyProperty =
            DependencyProperty.Register("IsReadOnly", typeof (bool), typeof (EditableTextBlock), new PropertyMetadata(default(bool)));

        /// <summary>
        ///   If the control is read only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return (bool) GetValue(IsReadOnlyProperty); }
            set { SetValue(IsReadOnlyProperty, value); }
        }

        /// <summary>
        ///   Dependency property for the text property.
        /// </summary>
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof (string), typeof (EditableTextBlock), new PropertyMetadata(default(string)));

        /// <summary>
        ///   Text contents of the editable text block
        /// </summary>
        public string Text
        {
            get { return (string) GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        #endregion

        

        /// <summary>
        ///   Constructor
        /// </summary>
        public EditableTextBlock()
        {
            InitializeComponent();
            Focusable = true;
            FocusVisualStyle = null;
        }

        #region Event handlers

        /// <summary>
        ///   Event handler for when a textbox loses focus.
        /// </summary>
        /// <param name="sender">Textbox that lost focus.</param>
        /// <param name="e">Routed event args.</param>
        private void TextBoxLostFocus(object sender, RoutedEventArgs e)
        {
            var textbox = (TextBox)sender;
            var bindingExpression = textbox.GetBindingExpression(TextBox.TextProperty);

            // save the editing by forcing a write to the binding source
            if (bindingExpression != null)
            {
                bindingExpression.UpdateSource();
            }

            IsEditable = false;
        }


        /// <summary>
        ///   Event handler for when a key is pressed on the textbox
        /// </summary>
        /// <param name="sender">The textbox that had keyboard focus when the key was pressed.</param>
        /// <param name="e">Key event args.</param>
        private void TextBoxKeyDown(object sender, KeyEventArgs e)
        {
            var textbox = (TextBox) sender;
            var bindingExpression = textbox.GetBindingExpression(TextBox.TextProperty);
            switch(e.Key)
            {
                case(Key.Escape): // cancel
                    
                    // undo the editing by forcing a read from the binding source
                    if (bindingExpression != null)
                    {
                        bindingExpression.UpdateTarget();
                    }
                    IsEditable = false;
                    Focus();
                    break;
                case(Key.Enter): // save
                    

                    // save the editing by forcing a write to the binding source
                    if (bindingExpression != null)
                    {
                        bindingExpression.UpdateSource();
                    }
                    IsEditable = false;
                    Focus();
                    break;
            }
        }

        /// <summary>
        ///   Event handler for when the textbox is loaded.
        /// </summary>
        /// <param name="sender">The textbox that was loaded.</param>
        /// <param name="e">The routed event arguments.</param>
        private void TextBoxLoaded(object sender, RoutedEventArgs e)
        {
            var textBox = (TextBox) sender;
            var bindingExpression = textBox.GetBindingExpression(TextBox.TextProperty);
            
            // tell the binding expression to read from the source
            if (bindingExpression != null)
            {
                bindingExpression.UpdateTarget();
            }

            textBox.Focus();
            textBox.SelectAll();
        }

        #endregion
    }
}
