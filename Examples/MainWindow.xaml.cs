﻿using System.Windows;

namespace netstations.Examples
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Item rootItem = new Item("Root Item");

            rootItem.Children.Add(new Item("Sub Item"));
            rootItem.Children.Add(new Item("Sub Item2"));
            rootItem.Children[1].Children.Add(new Item("SubSub Item"));
            rootItem.Children[1].Children.Add(new Item("SubSub Item2"));
            rootItem.Children[1].Children.Add(new Item("SubSub Item3"));
            rootItem.Children.Add(new Item("Sub Item3"));
            rootItem.Children.Add(new Item("Sub Item4"));
            rootItem.Children.Add(new Item("Sub Item5"));

            TView.ItemsSource = rootItem.Children;
        }
    }
}
